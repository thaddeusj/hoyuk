#include "sdlthread.h"

SDLThread::SDLThread(QApplication *app, QWaitCondition *waitInit) : QThread(0) {
    //////// BEGIN INITIALIZE STATE ////////

    // General Configuration
    windowTitle = "Hoyuk Game Engine";
    iconTitle = "Hoyuk";
    this->waitInit = waitInit;

    // Default display size
    displayWidth = 1280;
    displayHeight = 720;
    fullScreen = false;
    camera = Camera(displayWidth, displayHeight);
    cameraLogicModule = NULL;
    cameraLogic = NULL;

    // Simulation parameters
    doRun = true;

    // Simulation state
    pauseLoop = true;
    pauseRender = true;
    csets = new ControlSet[4];
    worldStart = 0;
    worldTime = 0;

    // Other state
    this->app = app;
    errorCode = SDLTHREAD_OK;
    errorString = QString();

    // Scripting
    scr = asCreateScriptEngine(ANGELSCRIPT_VERSION);
    ScriptUtil::instance().setScriptEngine(scr);
    scrCtx = NULL;

    // Control
    globalLoopLock = false;

    ///////// END INITIALIZE STATE /////////
}

void SDLThread::run() {
    if (init() == false)
        return;

    SDL_Event ev;
    quint64 nanos;
    float32 timeStep;
    float32 fixStep = 1.0f / 60.0f;

    // Prime the simulation with one call to onLoop() and reset the timer
    onLoop(fixStep);
    HoyukTime::reset();

    while (doRun) {
        // Global loop lock for development
        if (globalLoopLock)
            globalLoopMutex.lock();

        // Poll events
        while (SDL_PollEvent(&ev)) {
            onEvent(&ev);
        }

        // Run logic and rendering if we have an initialized world
        if (hworld.isInitialized()) {
            worldTime = SDL_GetTicks() - worldStart;

            if (!pauseLoop) {
                nanos = HoyukTime::nanosElapsed();

                //if (nanos >= 8333333) {
                if (nanos >= 16666666) {
                    timeStep = (float32)nanos / 1000000000.0f;
                    onLoop(timeStep); // Compute next state
                    HoyukTime::reset();

                    if (!pauseRender)
                        render(); // Draw simulation state
                }
            }
        } else {
            QThread::msleep(5);
        }

        // Global loop unlock
        if (globalLoopLock)
            globalLoopMutex.unlock();

        // Handle any exteral tasks that have been requested
        tasksMutex.lock();
        foreach (ExtTask *task, tasks) {
            if (task != NULL) {
                BlockingExtTask *blockingTask = dynamic_cast<BlockingExtTask*>(task);

                if (blockingTask != NULL) {
                    // qDebug() << "SDLThread executing BLOCKING task: " << task->name;
                    (*blockingTask)();
                } else {
                    // qDebug() << "SDLThread executing task: " << task->name;
                    (*task)();
                    delete task;
                }
            }
        }
        tasks.clear();
        tasksMutex.unlock();

        QThread::yieldCurrentThread();
    }

    cleanup();
}

void SDLThread::shutdown() {
    doRun = false;
}

bool SDLThread::init() {
    // Init timing
    HoyukTime::initTime();

    // Init scripting
    scr->SetMessageCallback(asFUNCTION(&SDLThread::ASMessageCallback), 0, asCALL_CDECL);
    RegisterQString(scr);
    RegisterBox2D(scr);
    HWorld::ASRegister(scr);
    scr->RegisterGlobalFunction("void asprint(const string &in)", asFUNCTION(&SDLThread::asprint), asCALL_CDECL);

    // Init media layer
    camera.width(10);

    if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
        errorCode = SDLTHREAD_SDL_INIT_FAIL;
        errorString = "SDL init failed";
        return false;
    }

    // Setup OpenGL Parameters
    SDL_GL_SetAttribute(SDL_GL_RED_SIZE,         8);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE,       8);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE,        8);
    SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE,       8);

    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE,       16);
    SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE,      32);

    SDL_GL_SetAttribute(SDL_GL_ACCUM_RED_SIZE,   8);
    SDL_GL_SetAttribute(SDL_GL_ACCUM_GREEN_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_ACCUM_BLUE_SIZE,  8);
    SDL_GL_SetAttribute(SDL_GL_ACCUM_ALPHA_SIZE, 8);

    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS,  1);
    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES,  2);
    SDL_GL_SetAttribute(SDL_GL_SWAP_CONTROL, 0);

    // Setup window manager info
    SDL_WM_SetCaption(windowTitle.toLocal8Bit().data(), iconTitle.toLocal8Bit().data());

    //surface = SDL_SetVideoMode(displayWidth, displayHeight, 32, SDL_HWSURFACE | SDL_GL_DOUBLEBUFFER | SDL_OPENGL | SDL_FULLSCREEN);
    surface = SDL_SetVideoMode(displayWidth, displayHeight, 32, SDL_HWSURFACE | SDL_GL_DOUBLEBUFFER | SDL_OPENGL | SDL_NOFRAME);
    if (surface == NULL) {
        return false;
    }

    // Init GL view area
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glViewport(0, 0, displayWidth, displayHeight);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, displayWidth, displayHeight, 0, 1, -1);

    glMatrixMode(GL_MODELVIEW);
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    SDL_GL_SwapBuffers();

    glLoadIdentity();

    GLenum glError = glGetError();
    if (glError != GL_NO_ERROR) {
        errorCode = SDLTHREAD_GL_INIT_FAIL;
        // errorString = QString(gluErrorString(glError)); // TODO - Get glut or something so we can call this
        errorString = "OpenGL Init Failed";
        return false;
    }

    // Init inputs
    SDL_JoystickEventState(SDL_ENABLE);
    joy0 = SDL_JoystickOpen(0);

    // Notify listeners that SDLThread init is complete
    waitInit->wakeAll();

    return true;
}

void SDLThread::onEvent(SDL_Event *ev) {
    if (ev->type == SDL_QUIT) {
        shutdown();
    } else if (ev->type == SDL_JOYAXISMOTION) {
        ControlSet *cset = csets + ev->jaxis.which;

        if (ev->jaxis.axis == 0) {
            cset->x = ev->jaxis.value;
        } else if (ev->jaxis.axis == 1) {
            cset->y = ev->jaxis.value;
        }
    } else if (ev->type == SDL_JOYBUTTONDOWN) {
        csets[ev->jbutton.which].btn(ev->jbutton.button, true);
    } else if (ev->type == SDL_JOYBUTTONUP) {
        csets[ev->jbutton.which].btn(ev->jbutton.button, false);
    } else if (ev->type == SDL_KEYDOWN) {
        if (ev->key.keysym.sym == SDLK_a) {
            csets[0].x = SHRT_MIN;
        } else if (ev->key.keysym.sym == SDLK_d) {
            csets[0].x = SHRT_MAX;
        } else if (ev->key.keysym.sym == SDLK_SPACE) {
            csets[0].btn(0, true);
        }
    } else if (ev->type == SDL_KEYUP) {
        if (ev->key.keysym.sym == SDLK_a || ev->key.keysym.sym == SDLK_d) {
            csets[0].x = 0;
        } else if (ev->key.keysym.sym == SDLK_SPACE) {
            csets[0].btn(0, false);
        } else if (ev->key.keysym.sym == SDLK_ESCAPE) {
            doRun = false;
        }
    }
}

void SDLThread::onLoop(float32 timeStep) {
    int r; // Return value for AngelScript API calls
    ControlSet *cset; // Control set pointer passed to ActorInstance.onLoop()

    // See if any buttons changed
    for (unsigned cidx = 0; cidx < 4; cidx++) {
        for (unsigned bidx = 0; bidx < ControlSet::btnCount; bidx++) {
            csets[cidx].btnChange(bidx, (csets[cidx].btn(bidx) != csets[cidx].btnLast(bidx)));
        }
    }

    // Run actor behaviors
    QList<ActorInstance*>::iterator actInstIt;
    for (actInstIt = hworld.actorInstances.begin(); actInstIt != hworld.actorInstances.end(); ++actInstIt) {
        ActorInstance *actInst = *actInstIt;

        if (actInst->controlSet > 0) {
            cset = csets + (actInst->controlSet - 1);
        } else {
            cset = NULL;
        }

        actInst->onLoop(timeStep, cset);
    }

    // Step the physics
    hworld.world()->Step(timeStep, 8, 4);
    hworld.world()->ClearForces();

    // Update control button last states
    for (unsigned cidx = 0; cidx < 4; cidx++) {
        for (unsigned bidx = 0; bidx < ControlSet::btnCount; bidx++) {
            csets[cidx].btnLast(bidx, csets[cidx].btn(bidx));
        }
    }

    // Update the camera
    if (cameraLogic != NULL) {
        scrCtx->Prepare(cameraLogic);
        scrCtx->SetArgAddress(0, &camera);
        r = scrCtx->Execute();
        if (r != asEXECUTION_FINISHED) {
            qDebug() << "SDLThread execute script function cameraLogic(Camera@) failed";
            cameraLogic = NULL;
        }
    }
}

void SDLThread::render() {
    glMatrixMode(GL_MODELVIEW);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    glScaled(camera.xScale(), camera.yScale(), 1); // Scale meters to pixels
    glTranslated(camera.x() * -1, camera.y() * -1, 0); // Position camera over scene

    //
    // Render Visible Backgrounds
    //
    QList<BackgroundInstance>::iterator bgInstIt;
    for (bgInstIt = hworld.backgroundInstances.begin(); bgInstIt != hworld.backgroundInstances.end(); ++bgInstIt) {
        BackgroundInstance &bgInst = *bgInstIt;

        if (SDLThread::qcIntersect(camera.rect(), bgInst.rect())) {
            Background *bg = bgInst.getBackground();

            glPushMatrix();

            const TextureRect &trect = bg->getTextureRect();

            GLdouble bgWidth = bg->getWidth();
            GLdouble bgHeight = bg->getHeight();

            glTranslated(bgInst.x, bgInst.y, 0.99);
            glBindTexture(GL_TEXTURE_2D, bg->getTexID());

            glBegin(GL_QUADS);
            glTexCoord2d(trect.left, trect.top); glVertex3d(0, 0, 0);
            glTexCoord2d(trect.right, trect.top); glVertex3d(bgWidth, 0, 0);
            glTexCoord2d(trect.right, trect.bottom); glVertex3d(bgWidth, bgHeight, 0);
            glTexCoord2d(trect.left, trect.bottom); glVertex3d(0, bgHeight, 0);
            glEnd();

            glPopMatrix();
        }
    }

    //
    // Render Visible Actors
    //
    QList<ActorInstance*>::iterator actInstIt;
    for (actInstIt = hworld.actorInstances.begin(); actInstIt != hworld.actorInstances.end(); ++actInstIt) {
        ActorInstance *actInst = *actInstIt;
        if (actInst->bodyPtr() == NULL)
            continue;

        b2Vec2 actPos = actInst->bodyPtr()->GetPosition();
        b2Vec2 actVel = actInst->bodyPtr()->GetLinearVelocity();

        glPushMatrix();
        const QuadCoords &qCoords = actInst->getActor()->getQuadCoords();

        TextureRect trect = actInst->getAnimationState().getTextureRect();

        // Do X directional flip is relevant
        GLdouble flipTemp;
        if (actInst->getActor()->getDirectionalX()) {
            if (actVel.x < 0) {
                flipTemp = trect.left;
                trect.left = trect.right;
                trect.right = flipTemp;
                actInst->lastDirX = actVel.x;
            } else if (actVel.x > 0) {
                actInst->lastDirX = actVel.x;
            } else if (actVel.x == 0 && actInst->lastDirX < 0) {
                flipTemp = trect.left;
                trect.left = trect.right;
                trect.right = flipTemp;
            }
        }

        // Do Y directional flip is relevant
        if (actInst->getActor()->getDirectionalY()) {
            if (actVel.y < 0) {
                flipTemp = trect.top;
                trect.top = trect.bottom;
                trect.bottom = flipTemp;
                actInst->lastDirY = actVel.y;
            } else if (actVel.y > 0) {
                actInst->lastDirY = actVel.y;
            } else if (actVel.y == 0 && actInst->lastDirY < 0) {
                flipTemp = trect.top;
                trect.top = trect.bottom;
                trect.bottom = flipTemp;
            }
        }

        glTranslated(actPos.x, actPos.y, 0);
        glRotated(actInst->bodyPtr()->GetAngle() * 57.2957795, 0, 0, 1);
        glBindTexture(GL_TEXTURE_2D, actInst->getTexID());

        glBegin(GL_QUADS);
        glTexCoord2d(trect.left, trect.top); glVertex3d(qCoords.left, qCoords.top, 0);
        glTexCoord2d(trect.right, trect.top); glVertex3d(qCoords.right, qCoords.top, 0);
        glTexCoord2d(trect.right, trect.bottom); glVertex3d(qCoords.right, qCoords.bottom, 0);
        glTexCoord2d(trect.left, trect.bottom); glVertex3d(qCoords.left, qCoords.bottom, 0);
        glEnd();

        glPopMatrix();
    }

    glFinish();
    SDL_GL_SwapBuffers();
}

void SDLThread::cleanup() {
    if (joy0 != NULL)
        SDL_JoystickClose(joy0);

    destroyScriptContext();

    if (scr != NULL)
        scr->Release();

    SDL_Quit();
    app->quit();
}

void SDLThread::createScriptContext() {
    if (scrCtx == NULL)
        scrCtx = scr->CreateContext();
    ScriptUtil::instance().scrCtx(scrCtx);
}

void SDLThread::destroyScriptContext() {
    cameraLogicModule = NULL;
    cameraLogic = NULL;

    if (scrCtx != NULL) {
        scrCtx->Release();
        scrCtx = NULL;
    }

    ScriptUtil::instance().scrCtx(NULL);
}

asIScriptEngine* SDLThread::scriptEngine() {
    return scr;
}

asIScriptContext* SDLThread::scriptContext() {
    return scrCtx;
}

void SDLThread::addTask(ExtTask *task) {
    tasksMutex.lock();
    tasks.append(task);
    tasksMutex.unlock();
}

void SDLThread::setPauseLoop(bool pauseLoop) {
    this->pauseLoop = pauseLoop;
}

void SDLThread::setPauseRender(bool pauseRender) {
    this->pauseRender = pauseRender;
}

void SDLThread::setGlobalLoopLock(bool globalLoopLock) {
    this->globalLoopLock = globalLoopLock;
}

QMutex& SDLThread::getGlobalLoopMutex() {
    return globalLoopMutex;
}

void SDLThread::unloadActor(const QString &actorName) {
    // TODO - If the world is initialized, purge any instances of this actor

    if (actors.contains(actorName)) {
        Actor *actor = actors[actorName];
        actors.remove(actorName);
        if (actor != NULL)
            delete actor;

        qDebug() << "SDLThread Unloaded actor:" << actorName;
    }
}

void SDLThread::unloadBackground(const QString &bgName) {
    // TODO - If the world is initialized, purge any instances of this background

    if (backgrounds.contains(bgName)) {
        Background *background = backgrounds[bgName];
        backgrounds.remove(bgName);
        if (background != NULL)
            delete background;

        qDebug() << "SDLThread Unloaded background:" << bgName;
    }
}

void SDLThread::unloadAllActors() {
    foreach (QString actorName, actors.keys()) {
        unloadActor(actorName);
    }
}

void SDLThread::unloadAllBackgrounds() {
    foreach (QString bgName, backgrounds.keys()) {
        unloadBackground(bgName);
    }
}

void SDLThread::unloadAllLibraries() {
    unloadAllBackgrounds();
    unloadAllActors();
}

void SDLThread::addActor(Actor *actor) {
    actors.insert(actor->getName(), actor);
}

void SDLThread::addBackground(Background *background) {
    backgrounds.insert(background->getName(), background);
}

QHash<QString, Actor*>& SDLThread::getActors() {
    return actors;
}

QHash<QString, Background*>& SDLThread::getBackgrounds() {
    return backgrounds;
}

HWorld& SDLThread::getHWorld(bool newWorld){
    //
    // If newWorld true, destroy all existing world data and reset the camera
    //
    if (newWorld) {
        setCameraLogicModule(NULL);
        hworld.destroy();
        destroyScriptContext();
    }

    return hworld;
}

void SDLThread::resetWorldStartTime() {
    worldStart = SDL_GetTicks();
    worldTime = 0;
}

Camera* SDLThread::getCamera() {
    return &camera;
}

QSize SDLThread::getDisplaySize() {
    return QSize(displayWidth, displayHeight);
}

void SDLThread::setCameraLogicModule(asIScriptModule *cameraLogicModule) {
    this->cameraLogicModule = cameraLogicModule;
    if (cameraLogicModule != NULL) {
        cameraLogic = cameraLogicModule->GetFunctionByDecl("void cameraLogic(Camera@)");
    } else {
        cameraLogic = NULL;
    }
}

asIScriptModule* SDLThread::getCameraLogicModule() {
    return cameraLogicModule;
}

void SDLThread::ASMessageCallback(const asSMessageInfo *msg, void *param) {
    QString msgType = "ERROR";
    if (msg->type == asMSGTYPE_WARNING)
        msgType = "WARN";
    else if (msg->type == asMSGTYPE_INFORMATION)
        msgType = "INFO";

    qDebug() << msg->section << " line " << msg->row << " col " << msg->col << msgType << msg->message;
}

void SDLThread::asprint(const QString &msg) {
    qDebug() << QString("SCRIPT: ").append(msg);
}

bool SDLThread::qcIntersect(const QuadCoords &a, const QuadCoords &b) {
    return ! (b.top > a.bottom ||
              b.bottom < a.top ||
              b.left > a.right ||
              b.right < a.left);
}
