#include "scriptbox2d.h"

static void ConstructB2Vec2(b2Vec2 *thisPointer) {
    new(thisPointer) b2Vec2();
}

static void ConstructB2Vec2(float32 x, float32 y, b2Vec2 *thisPointer) {
    new(thisPointer) b2Vec2(x, y);
}

static void RegisterB2Vec2(asIScriptEngine *engine) {
    int r;
    r = engine->RegisterObjectType("b2Vec2", sizeof(b2Vec2), asOBJ_VALUE | asOBJ_POD | asOBJ_APP_CLASS_C); assert(r >= 0);
    r = engine->RegisterObjectBehaviour("b2Vec2", asBEHAVE_CONSTRUCT, "void f()", asFUNCTIONPR(ConstructB2Vec2, (b2Vec2*), void), asCALL_CDECL_OBJLAST); assert(r >= 0);
    r = engine->RegisterObjectBehaviour("b2Vec2", asBEHAVE_CONSTRUCT, "void f(float, float)", asFUNCTIONPR(ConstructB2Vec2, (float32, float32, b2Vec2*), void), asCALL_CDECL_OBJLAST); assert(r >= 0);
    r = engine->RegisterObjectProperty("b2Vec2", "float x", asOFFSET(b2Vec2, x)); assert(r >= 0);
    r = engine->RegisterObjectProperty("b2Vec2", "float y", asOFFSET(b2Vec2, y)); assert(r >= 0);
    r = engine->RegisterObjectMethod("b2Vec2", "void SetZero()", asMETHOD(b2Vec2, SetZero), asCALL_THISCALL); assert(r >= 0);
    r = engine->RegisterObjectMethod("b2Vec2", "void Set(float, float)", asMETHODPR(b2Vec2, Set, (float, float), void), asCALL_THISCALL); assert(r >= 0);
}

static void RegisterB2Body(asIScriptEngine *engine) {
    int r;
    r = engine->RegisterObjectType("b2Body", 0, asOBJ_REF | asOBJ_NOCOUNT); assert(r >= 0);
    r = engine->RegisterObjectMethod("b2Body", "const b2Vec2 &GetPosition() const", asMETHOD(b2Body, GetPosition), asCALL_THISCALL); assert(r >= 0);
    r = engine->RegisterObjectMethod("b2Body", "float GetAngle() const", asMETHOD(b2Body, GetAngle), asCALL_THISCALL); assert(r >= 0);
    r = engine->RegisterObjectMethod("b2Body", "b2Vec2 GetLinearVelocity() const", asMETHOD(b2Body, GetLinearVelocity), asCALL_THISCALL); assert(r >= 0);
    r = engine->RegisterObjectMethod("b2Body", "void SetLinearVelocity(b2Vec2 &in)", asMETHOD(b2Body, SetLinearVelocity), asCALL_THISCALL); assert(r >= 0);
    r = engine->RegisterObjectMethod("b2Body", "float GetAngularVelocity() const", asMETHOD(b2Body, GetAngularVelocity), asCALL_THISCALL); assert(r >= 0);
    r = engine->RegisterObjectMethod("b2Body", "void ApplyForce(const b2Vec2 &in, const b2Vec2 &in)", asMETHOD(b2Body, ApplyForce), asCALL_THISCALL); assert(r >= 0);
    r = engine->RegisterObjectMethod("b2Body", "void ApplyTorque(float)", asMETHOD(b2Body, ApplyTorque), asCALL_THISCALL); assert(r >= 0);
    r = engine->RegisterObjectMethod("b2Body", "void ApplyLinearImpulse(const b2Vec2 &in, const b2Vec2 &in)", asMETHOD(b2Body, ApplyLinearImpulse), asCALL_THISCALL); assert(r >= 0);
    r = engine->RegisterObjectMethod("b2Body", "void ApplyAngularImpulse(float)", asMETHOD(b2Body, ApplyAngularImpulse), asCALL_THISCALL); assert(r >= 0);
}

void RegisterBox2D(asIScriptEngine *engine) {
    RegisterB2Vec2(engine);
    RegisterB2Body(engine);
}
