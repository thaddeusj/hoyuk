#include "scriptutil.h"

ScriptUtil::ScriptUtil() {
}

void ScriptUtil::setScriptEngine(asIScriptEngine *scr) {
    this->scr = scr;
}

asIScriptContext* ScriptUtil::scrCtx() {
    return _scrCtx;
}

void ScriptUtil::scrCtx(asIScriptContext *_scrCtx) {
    this->_scrCtx = _scrCtx;
}

bool ScriptUtil::buildScriptModule(const QString &moduleName, const char *src) {
    if (scr == NULL)
        return false;

    CScriptBuilder scrBuilder;
    int r;

    r = scrBuilder.StartNewModule(scr, moduleName.toUtf8().data());
    if (r < 0)
        return false;

    r = scrBuilder.AddSectionFromMemory(src);
    if (r < 0)
        return false;

    r = scrBuilder.BuildModule();
    if (r < 0)
        return false;

    return true;
}

bool ScriptUtil::buildScriptModule(const QString &moduleName, const QStringList &fileList) {
    if (scr == NULL)
        return false;

    QString src;

    foreach (QString filePath, fileList) {
        QFile file(filePath);
        if (file.open(QIODevice::ReadOnly | QIODevice::Text)) {
            QByteArray data = file.readAll();
            if (data.size() == 0)
                qDebug() << "WARNING: readAll() resulted in 0 bytes for file: " << filePath;
            src.append(data).append('\n');
        } else {
            qDebug() << QString("Failed to open script source file: ").append(filePath);
            return false;
        }
    }

    return buildScriptModule(moduleName, src.toLocal8Bit().data());
}

asIScriptModule* ScriptUtil::getModule(const char *moduleName) {
    if (scr == NULL)
        return NULL;
    return scr->GetModule(moduleName);
}

asIScriptModule* ScriptUtil::getModule(const QString &moduleName) {
    return getModule(moduleName.toLocal8Bit().data());
}
