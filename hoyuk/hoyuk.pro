#-------------------------------------------------
#
# Project created by QtCreator 2012-12-16T21:52:31
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = hoyuk
TEMPLATE = app

SOURCES += main.cpp\
        sdlthread.cpp \
    model/actor.cpp \
    resourceloader.cpp \
    model/animationstate.cpp \
    devui/developerwindow.cpp \
    model/background.cpp \
    model/texturerect.cpp \
    resourceloader/archivereader.cpp \
    resourceloader/actorreader.cpp \
    resourceloader/loadresult.cpp \
    resourceloader/imagetools.cpp \
    resourceloader/backgroundreader.cpp \
    model/camera.cpp \
    model/hworld.cpp \
    worldloader.cpp \
    model/controlset.cpp \
    logic/cameralogic.cpp \
    logic/followxlogic.cpp \
    asaddon/serializer.cpp \
    asaddon/scriptstdstring_utils.cpp \
    asaddon/scriptstdstring.cpp \
    asaddon/scriptmathcomplex.cpp \
    asaddon/scriptmath.cpp \
    asaddon/scripthelper.cpp \
    asaddon/scripthandle.cpp \
    asaddon/scriptfile.cpp \
    asaddon/scriptdictionary.cpp \
    asaddon/scriptbuilder.cpp \
    asaddon/scriptarray.cpp \
    asaddon/scriptany.cpp \
    asaddon/debugger.cpp \
    asaddon/contextmgr.cpp \
    asaddon/scriptqstring.cpp \
    asaddon/scriptbox2d.cpp \
    resourceloader/resourceentry.cpp \
    scriptutil.cpp \
    devui/windowpostask.cpp \
    devui/developerdock.cpp \
    devui/titlebarlabel.cpp

win32: SOURCES += time/hoyuktime-win32.cpp
else:unix: SOURCES += time/hoyuktime-posix.cpp

HEADERS  +=\
            sdlthread.h \
    model/actor.h \
    resourceloader.h \
    model/animationstate.h \
    devui/developerwindow.h \
    model/quadcoords.h \
    model/background.h \
    model/texturerect.h \
    resourceloader/archivereader.h \
    resourceloader/actorreader.h \
    resourceloader/loadresult.h \
    resourceloader/imageresource.h \
    resourceloader/frameresource.h \
    resourceloader/imagetools.h \
    resourceloader/backgroundreader.h \
    devui/loadactortask.h \
    devui/loadbackgroundtask.h \
    model/camera.h \
    model/hworld.h \
    worldloader.h \
    model/controlset.h \
    logic/cameralogic.h \
    logic/followxlogic.h \
    asaddon/serializer.h \
    asaddon/scriptstdstring.h \
    asaddon/scriptmathcomplex.h \
    asaddon/scriptmath.h \
    asaddon/scripthelper.h \
    asaddon/scripthandle.h \
    asaddon/scriptfile.h \
    asaddon/scriptdictionary.h \
    asaddon/scriptbuilder.h \
    asaddon/scriptarray.h \
    asaddon/scriptany.h \
    asaddon/debugger.h \
    asaddon/contextmgr.h \
    asaddon/aswrappedcall.h \
    asaddon/scriptqstring.h \
    asaddon/scriptbox2d.h \
    resourceloader/resourceentry.h \
    time/hoyuktime.h \
    scriptutil.h \
    devui/setgloballooplocktask.h \
    devui/windowpostask.h \
    devui/developerdock.h \
    devui/titlebarlabel.h

FORMS    += \
    developerwindow.ui \
    devui/developerdock.ui

# Get dem warnings!
win32:QMAKE_CXXFLAGS_WARN_ON = /W3

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../libs/release/ -lSDL -lopengl32 -lFreeImage -lBox2D -lqjson -larchive -langelscript
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../libs/debug/ -lSDL -lopengl32 -lFreeImage -lBox2D -lqjson -larchive -langelscriptd
else:unix: LIBS += -L$$PWD/../libs/ -lSDL -lopengl32 -lFreeImage -lBox2D -lqjson -larchive -langelscriptd

win32: LIBS += -luser32

INCLUDEPATH += $$PWD/../libs/include
DEPENDPATH += $$PWD/../libs/include

RESOURCES += \
    devui/icons.qrc
