#ifndef SDLTHREAD_H
#define SDLTHREAD_H

#include <limits.h>
#include <QtCore>
#include <QApplication>
#include <SDL.h>
#include <GL/glew.h>
#include <angelscript.h>

#include "time/hoyuktime.h"
#include "scriptutil.h"

#include "asaddon/scriptqstring.h"
#include "asaddon/scriptbuilder.h"
#include "asaddon/scriptbox2d.h"

#include "model/hworld.h"
#include "logic/cameralogic.h"

#define SDLTHREAD_OK 0
#define SDLTHREAD_SDL_INIT_FAIL 1
#define SDLTHREAD_GL_INIT_FAIL 2

#define ASMN_CAMERA_LOGIC "CameraLogic"

/*!
 * \brief The ExtTask class - For passing executable tasks to the SDL loop
 */
class ExtTask {
public:
    QString name;

    ExtTask(const QString &name) {
        this->name = QString(name);
    }

    virtual void run() = 0;

    void operator ()() {
        run();
    }
};

/*!
 * \brief The BlockingExtTask class - For passing executable tasks to the SDL loop, and providing
 * a method that blocks until the task has been executed
 *
 * \note A BlockingExtTask will never be executed by the SDL loop if wait() is not called on it.
 */
class BlockingExtTask : public ExtTask {
protected:
    QMutex waitMutex;
    QWaitCondition waitTaskExecuted;

public:
    BlockingExtTask(const QString &name) : ExtTask(name) {
        waitMutex.lock();
    }

    void operator ()() {
        waitMutex.lock();
        run();
        waitTaskExecuted.wakeAll();
        waitMutex.unlock();
    }

    void wait() {
        waitTaskExecuted.wait(&waitMutex);
    }
};

class SDLThread : public QThread {
    Q_OBJECT

private:
    // General Configuration
    QString windowTitle;
    QString iconTitle;
    QWaitCondition *waitInit;

    // Libraries
    QHash<QString, Actor*> actors;
    QHash<QString, Background*> backgrounds;

    // Display settings
    int displayWidth; // pixels
    int displayHeight; // pixels
    bool fullScreen;

    Camera camera;
    asIScriptModule *cameraLogicModule;
    asIScriptFunction *cameraLogic;

    // State
    QApplication *app;
    bool doRun;
    SDL_Surface *surface;

    HWorld hworld;
    unsigned long worldStart;
    unsigned long worldTime;

    bool pauseLoop; // Loop paused
    bool pauseRender; // Render paused

    int errorCode;
    QString errorString;

    SDL_Joystick *joy0;
    SDL_Joystick *joy1;
    SDL_Joystick *joy2;
    SDL_Joystick *joy3;

    ControlSet *csets;

    // Scripting
    asIScriptEngine *scr;
    asIScriptContext *scrCtx;

    // Simulation functions
    bool init();
    void onEvent(SDL_Event *ev);
    void onLoop(float32 timeStep);
    void render();
    void cleanup();

    // Control
    QMutex tasksMutex;
    QList<ExtTask*> tasks;
    QMutex globalLoopMutex;
    bool globalLoopLock;

public:
    explicit SDLThread(QApplication *app, QWaitCondition *waitInit);
    void run();

    // Library access
    QHash<QString, Actor*>& getActors();
    QHash<QString, Background*>& getBackgrounds();
    void unloadActor(const QString &actorName);
    void unloadBackground(const QString &bgName);
    void unloadAllActors();
    void unloadAllBackgrounds();
    void unloadAllLibraries();
    void addActor(Actor *actor); // deprecated
    void addBackground(Background *background); // deprecated

    // Control
    void shutdown();
    void setPauseLoop(bool pauseLoop);
    void setPauseRender(bool pauseRender);
    void setGlobalLoopLock(bool globalLoopLock);
    QMutex& getGlobalLoopMutex();
    void addTask(ExtTask *task);
    void setCameraLogicModule(asIScriptModule *cameraLogicModule);
    asIScriptModule* getCameraLogicModule();
    void resetWorldStartTime();

    // State access
    HWorld& getHWorld(bool newWorld = false);
    Camera *getCamera();
    QSize getDisplaySize();

    // Scripting
    asIScriptEngine* scriptEngine();
    asIScriptContext* scriptContext();
    void createScriptContext();
    void destroyScriptContext();
    void static ASMessageCallback(const asSMessageInfo *msg, void *param);
    void static asprint(const QString &msg);

    // Utility
    static bool qcIntersect(const QuadCoords &a, const QuadCoords &b);
};

#endif // SDLTHREAD_H
