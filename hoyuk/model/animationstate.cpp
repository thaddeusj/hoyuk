#include "model\animationstate.h"

AnimationState::AnimationState() {
    this->null = true;

    texRect = TextureRect();
    frameRate = 0;
    numFrames = 0;
    frameTexIDs = NULL;
}

AnimationState::AnimationState(uint numFrames, const TextureRect & texRect, uint frameRate) {
    this->null = false;

    this->texRect = TextureRect(texRect);
    this->frameRate = frameRate;
    this->numFrames = numFrames;
    this->frameTexIDs = new GLuint[numFrames];
}

AnimationState::~AnimationState() {

}

bool AnimationState::isNull() {
    return null;
}

uint AnimationState::getFrameCount() {
    return numFrames;
}

const TextureRect& AnimationState::getTextureRect() const {
    return texRect;
}

void AnimationState::setFrameTexID(uint frameNum, GLuint texID) {
    frameTexIDs[frameNum] = texID;
}

GLuint AnimationState::getTexID(uint frameNum) {
    return frameTexIDs[frameNum];
}

uint AnimationState::getFrameRate() {
    return frameRate;
}

void AnimationState::unload() {
    if (numFrames > 0) {
        for (unsigned i = 0; i < numFrames; i++) {
            glDeleteTextures(1, frameTexIDs + i);
        }
    }

    numFrames = 0;
    frameTexIDs = NULL;
    frameRate = 0;
}
