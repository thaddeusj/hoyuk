#ifndef BACKGROUND_H
#define BACKGROUND_H

#include <QtCore>
#include <GL/glew.h>

#include "texturerect.h"
#include "quadcoords.h"

class Background {
private:
    QString name;
    GLdouble width;
    GLdouble height;
    GLuint texID;
    TextureRect trect;

public:
    Background(
        const QString &name,
        GLdouble width, GLdouble height,
        GLuint texID,
        const TextureRect &trect);

    const QString& getName() const;
    GLdouble getWidth();
    GLdouble getHeight();
    GLuint getTexID();
    const TextureRect& getTextureRect() const;
    void unload();
};

class BackgroundInstance {

private:
    Background *background;

public:
    GLdouble x;
    GLdouble y;

    QuadCoords rect() const;

    BackgroundInstance(Background *background, GLdouble x, GLdouble y);
    Background* getBackground();
};

#endif // BACKGROUND_H
