#include "texturerect.h"

TextureRect::TextureRect() {
    top = 0;
    bottom = 0;
    left = 0;
    right = 0;
}

TextureRect::TextureRect(const TextureRect &other) {
    top = other.top;
    bottom = other.bottom;
    left = other.left;
    right = other.right;
}

TextureRect::TextureRect(int pxres, int pxtop, int pxleft, int pxbottom, int pxright, bool invertY) {
    // Translate upper-left origin coords to lower-left origin
    pxtop = pxres - pxtop;
    pxbottom = pxres - pxbottom;

    // Calculate GLdouble values for provided pixel offsets
    top = (invertY ? (GLdouble)pxbottom : (GLdouble)pxtop) / (GLdouble)pxres;
    bottom = (invertY ? (GLdouble)pxtop : (GLdouble)pxbottom) / (GLdouble)pxres;
    left = (GLdouble)pxleft / (GLdouble)pxres;
    right = (GLdouble)pxright / (GLdouble)pxres;
}

