#include "resourceentry.h"

ResourceEntry::ResourceEntry(const QString &name, qint64 size) {
    this->_name = name;
    this->_size = size;
}

const QString& ResourceEntry::name() const {
    return _name;
}

qint64 ResourceEntry::size() const {
    return _size;
}

//
// ARCHIVE RESOURCE ENTRY
//

ArchiveResourceEntry::ArchiveResourceEntry(archive *ar, const QString &name, qint64 size) : ResourceEntry(name, size) {
    this->_ar = ar;
}

long ArchiveResourceEntry::read(char *buf, unsigned count) {
    return archive_read_data(_ar, buf, count);
}

//
// FILE RESOURCE ENTRY
//

FileResourceEntry::FileResourceEntry(const QString &name, QFile *file) : ResourceEntry(name, QFileInfo(*file).size()) {
    this->_file = file;
}

FileResourceEntry::~FileResourceEntry() {
    if (_file->isOpen())
        _file->close();
}

long FileResourceEntry::read(char *buf, unsigned count) {
    if (! _file->isOpen()) {
        if (! _file->open(QIODevice::ReadOnly)) {
            return -1;
        }
    }

    return _file->read((char*)buf, count);
}
