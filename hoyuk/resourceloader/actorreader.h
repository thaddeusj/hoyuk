#ifndef ACTORREADER_H
#define ACTORREADER_H

#include <limits.h>
#include <FreeImage.h>

#include "../scriptutil.h"

#include "frameresource.h"
#include "imagetools.h"
#include "../model/actor.h"

#include "archivereader.h"

class FrameBounds {
public:
    uint lowest;
    uint highest;

    FrameBounds() {
        lowest = UINT_MAX;
        highest = 0;
    }
};

class ActorReader : public ArchiveReader {

protected:
    static unsigned instCount;

    Actor *actor;
    asIScriptModule *scriptModule;

    QHash<QString, QList<FrameResource> > frameResources;
    QHash<QString, FrameBounds> frameBounds;
    QSet<QString> states;
    QVariantMap metaStates;

    QRegExp rxTexFileName;

    void handleEntry(ResourceEntry &re);
    void handleFrameEntry(const QString &state, unsigned frameNum, ResourceEntry &re);
    void handleScriptEntry(ResourceEntry &re);
    void postRead();
    void clean();

public:
    ActorReader();
    void readArchive(const QString& filePath);
    Actor* getActor();
};

#endif // ACTORREADER_H
