#ifndef BACKGROUNDREADER_H
#define BACKGROUNDREADER_H

#include <FreeImage.h>

#include "../model/background.h"
#include "archivereader.h"
#include "imageresource.h"
#include "imagetools.h"

class BackgroundReader : public ArchiveReader {

protected:
    Background *background;
    ImageResource bgImg;

    void handleEntry(ResourceEntry &re);
    void postRead();
    void clean();

public:
    BackgroundReader();
    Background* getBackground();
};

#endif // BACKGROUNDREADER_H
