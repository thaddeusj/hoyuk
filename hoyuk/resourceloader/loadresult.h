#ifndef LOADRESULT_H
#define LOADRESULT_H

#include <QtCore>

enum LoadErrorCode {
    OK,
    FILE_NOT_FOUND,
    LIBRARY_NOT_FOUND,
    ARCHIVE_OPEN_FAILED,
    BAD_META_DATA,
    JSON_PARSE_FAILED,
    IMAGE_EXTRACT_FAILED,
    IMAGE_LOAD_FAILED,
    IMAGE_RESOLUTION_MISMATCH,
    IMAGE_NOT_POW2,
    GPU_TEX_LOAD_FAIL,
    LIBRARY_RESOURCE_UNAVAILABLE,
    UNKNOWN_ERROR,
    UNEXPECTED_ERROR,
    OTHER_ERROR,
    IO_ERROR
};

class LoadResult {
public:
    LoadErrorCode errorCode;
    QString errorMessage;
    LoadResult();
};

#endif // LOADRESULT_H
