#include "imagetools.h"

FIBITMAP* loadBitmap(ImageResource &ir) {
    FreeImageIO io;
    io.read_proc = &ImageTools::lrImageRead;
    io.seek_proc = &ImageTools::lrImageSeek;
    io.tell_proc = &ImageTools::lrImageTell;

    FIBITMAP *bitmap;
    bitmap = FreeImage_LoadFromHandle(FIF_PNG, &io, &ir);
    return bitmap;
}

void createTexture(GLuint *texID, FIBITMAP* bitmap, unsigned texRes, LoadResult &loadResult) {
    if (bitmap == NULL) {
        loadResult.errorCode = OTHER_ERROR;
        loadResult.errorMessage = "Null pointer passed to createTexture() for bitmap";
        return;
    }

    GLsizei width = FreeImage_GetWidth(bitmap);
    GLsizei height = FreeImage_GetHeight(bitmap);

    if (width != height) {
        loadResult.errorCode = IMAGE_RESOLUTION_MISMATCH;
        loadResult.errorMessage = QString("Image is not square");
        return;
    }

    if (width != texRes) {
        loadResult.errorCode = IMAGE_RESOLUTION_MISMATCH;
        loadResult.errorMessage = QString("Image does not match specified resolution of ")
            .append(QString().setNum(texRes));
        return;
    }

    BYTE* rawData = FreeImage_GetBits(bitmap);

    glGenTextures(1, texID);
    glBindTexture(GL_TEXTURE_2D, *texID);
    // TODO - Portability, use BGRA on litte-endian platforms, RGBA on big-endian
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_BGRA, GL_UNSIGNED_BYTE, rawData);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glBindTexture(GL_TEXTURE_2D, NULL);

    if (glGetError() != GL_NO_ERROR) {
        loadResult.errorCode = GPU_TEX_LOAD_FAIL;
        loadResult.errorMessage = "ImageTools GPU texture upload failed";
    }
}

unsigned DLL_CALLCONV ImageTools::lrImageRead(void* buffer, unsigned size, unsigned count, fi_handle handle) {
    ImageResource *image = (ImageResource*)handle;
    unsigned bytes = size * count;

    if ((image->cursor + bytes) > image->pngDataSize) {
        qDebug() << "Asked for " << count << " items of " << size << " bytes, cursor: " << image->cursor << " pngDataSize: " << image->pngDataSize;
        count = (image->pngDataSize - image->cursor - 1) / size;
        bytes = size * count;
    }

    if (bytes == 0)
        return 0;

    memcpy(buffer, image->pngData + image->cursor, bytes);
    image->cursor += bytes;

    return count;
}

int DLL_CALLCONV ImageTools::lrImageSeek(fi_handle handle, long offset, int origin) {
    ImageResource *image = (ImageResource*)handle;

    if (origin == SEEK_CUR)
        image->cursor = image->cursor + offset;
    if (origin == SEEK_SET)
        image->cursor = offset;

    if (image->cursor >= image->pngDataSize)
        return 1;

    return 0;
}

long DLL_CALLCONV ImageTools::lrImageTell(fi_handle handle) {
    ImageResource *image = (ImageResource*)handle;
    return image->cursor;
}
