#ifndef IMAGERESOURCE_H
#define IMAGERESOURCE_H

class ImageResource {
public:
    unsigned char *pngData;
    unsigned long pngDataSize;
    unsigned long cursor; // For use by FreeImage read routines

    ImageResource() {
        this->cursor = 0;
        this->pngData = NULL;
        this->pngDataSize = 0;
    }

    ImageResource(const ImageResource &other) {
        pngData = other.pngData;
        pngDataSize = other.pngDataSize;
        cursor = other.cursor;
    }
};

#endif // IMAGERESOURCE_H
