#ifndef DEVELOPERDOCK_H
#define DEVELOPERDOCK_H

#include <QtGui>
#include "sdlthread.h"
#include "windowpostask.h"
#include "../worldloader.h"

namespace Ui {
    class DeveloperDock;
}

class DeveloperDock : public QMainWindow {
    Q_OBJECT

private:
    Ui::DeveloperDock *ui;

protected:
    QTimer initTimer;
    SDLThread *sdlThread;
    void setSDLWindowPos(QPoint newPos);
    
public:
    explicit DeveloperDock(SDLThread *sdlThread);
    ~DeveloperDock();

protected slots:
    void onTitleLabelDragged(QPoint newPos);
    void initTimerTimeout();

private slots:
    void on_exitButton_clicked();
    void on_loadWorldButton_clicked();
};

#endif // DEVELOPERDOCK_H
