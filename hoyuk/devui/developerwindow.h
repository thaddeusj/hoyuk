#ifndef DEVELOPERWINDOW_H
#define DEVELOPERWINDOW_H

#include <QtGui>
#include "../sdlthread.h"
#include "../worldloader.h"
#include "loadactortask.h"
#include "loadbackgroundtask.h"
#include "setgloballooplocktask.h"
#include "windowpostask.h"

namespace Ui {
    class DeveloperWindow;
}

class DeveloperWindow : public QMainWindow {
    Q_OBJECT

protected:
    // TODO - Add timer to snap developer window at startup
    QTimer snapTimer;

    void snapToSDLWindow();
    
public:
    explicit DeveloperWindow(SDLThread *sdlThread);
    ~DeveloperWindow();
    
private slots:
    void on_loadWorldButton_clicked();

    void on_globalLoopLockCheckBox_toggled(bool checked);

    void on_setPosButton_clicked();

    void on_snapButton_clicked();

    void onSnapTimerTimeout();

private:
    Ui::DeveloperWindow *ui;
    SDLThread *sdlThread;
};

#endif // DEVELOPERWINDOW_H
