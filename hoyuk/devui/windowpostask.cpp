#include "windowpostask.h"

#ifdef _WIN32
void WindowPosTask::setSDLWindowHandle(HWND sdlWindowHandle) {
    this->sdlWindowHandle = sdlWindowHandle;
    windowsFound += 1;
}

static BOOL CALLBACK EnumThreadWndProc(_In_ HWND hwnd, _In_ LPARAM lParam) {
    if (IsWindowVisible(hwnd) == TRUE) {
        WindowPosTask *task = (WindowPosTask*)lParam;
        task->setSDLWindowHandle(hwnd);
    }

    return TRUE;
}
#endif

WindowPosTask::WindowPosTask() : BlockingExtTask("WindowPosTask") {
    x = 0;
    y = 0;
    width = 0;
    height = 0;
    this->set = false;
}

WindowPosTask::WindowPosTask(int x, int y) : BlockingExtTask("WindowPosTask") {
    this->x = x;
    this->y = y;
    width = 0;
    height = 0;
    this->set = true;
}

int WindowPosTask::getX() {
    return x;
}

int WindowPosTask::getY() {
    return y;
}

int WindowPosTask::getWidth() {
    return width;
}

int WindowPosTask::getHeight() {
    return height;
}

void WindowPosTask::run() {
#ifdef _WIN32
    windowsFound = 0;
    DWORD threadId = GetCurrentThreadId();

    EnumThreadWindows(threadId, &EnumThreadWndProc, (LPARAM)this);
    if (windowsFound != 1) {
        qDebug() << "Number of windows owned by SDLThread != 1: " << windowsFound;
        return;
    }

    if (set) {
        SetWindowPos(sdlWindowHandle, NULL, x, y, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
    } else {
        WINDOWINFO wInfo;
        GetWindowInfo(sdlWindowHandle, &wInfo);
        x = wInfo.rcWindow.left;
        y = wInfo.rcWindow.top;
        width = wInfo.rcWindow.right - wInfo.rcWindow.left;
        height = wInfo.rcWindow.bottom - wInfo.rcWindow.top;
    }
#endif
}
