#ifndef LOADRESOURCETASK_H
#define LOADRESOURCETASK_H

#include "../sdlthread.h"
#include "../resourceloader.h"

class LoadActorTask : public ExtTask {
private:
    SDLThread *sdlThread;
    QString filePath;
public:
    LoadActorTask(SDLThread *sdlThread, const QString &filePath) : ExtTask("LoadActorTask") {
        this->sdlThread = sdlThread;
        this->filePath = filePath;
    }

    void run() {
        Actor *actor = loadActorResource(filePath);

        if (actor) {
            qDebug() << "LoadActorTask adding actor to runtime";
            sdlThread->addActor(actor);
        }
    }
};

#endif // LOADRESOURCETASK_H
