#include "titlebarlabel.h"

TitleBarLabel::TitleBarLabel(QWidget *parent) : QLabel(parent) {
    mouseDown = false;
}

void TitleBarLabel::mousePressEvent(QMouseEvent *ev) {
    if (ev->button() == Qt::LeftButton) {
        mouseDown = true;
        xOff = ev->x();
        yOff = ev->y();
    }
}

void TitleBarLabel::mouseReleaseEvent(QMouseEvent *ev) {
    if (ev->button() == Qt::LeftButton)
        mouseDown = false;
}

void TitleBarLabel::mouseMoveEvent(QMouseEvent *ev) {
    if (mouseDown) {
        QPoint newPos = QWidget::mapToGlobal(ev->pos());
        newPos.setX(newPos.x() - xOff);
        newPos.setY(newPos.y() - yOff);
        emit titleLabelDragged(newPos);
    }
}
