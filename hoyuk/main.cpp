#include <QtCore>
#include <QApplication>
#include "devui/developerwindow.h"
#include "devui/developerdock.h"
#include "sdlthread.h"

int main(int argc, char *argv[]) {
    QApplication a(argc, argv);
    a.setQuitOnLastWindowClosed(false);

    QMutex waitInitMutex;
    QWaitCondition waitInit;

    waitInitMutex.lock();
    SDLThread *sdlThread = new SDLThread(&a, &waitInit);

    //DeveloperWindow devWin(sdlThread);
    //devWin.show();
    DeveloperDock dock(sdlThread);
    dock.show();

    sdlThread->start();
    waitInit.wait(&waitInitMutex, 10000);
    
    int retVal = a.exec();

    delete sdlThread;
    return retVal;
}
