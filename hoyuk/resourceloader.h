#ifndef RESOURCELOADER_H
#define RESOURCELOADER_H

#include <QtCore>

#include "model/actor.h"
#include "model/animationstate.h"

#include "resourceloader/actorreader.h"
#include "resourceloader/backgroundreader.h"

Actor* loadActorResource(const QString &filePath);
Background* loadBackgroundResource(const QString &filePath);

#endif // RESOURCELOADER_H
