#include "worldloader.h"

WorldLoader::WorldLoader(SDLThread *sdlThread, const QString &filePath, const QString &libraryPath) {
    this->sdlThread = sdlThread;
    this->worldFile.setFileName(filePath);
    qDebug() << "worldFile " << QFileInfo(this->worldFile).absoluteFilePath();
    this->libraryDir.setPath(libraryPath);
    this->instID = 0;
    fullReload = false;
}

LoadResult WorldLoader::loadWorld(bool fullReload) {
    bool ok = false; // For number conversions
    int r; // For AngelScript API return values

    this->fullReload = fullReload;

    //
    // Basic Sanity Cheks
    //
    if (worldFile.exists() == false) {
        loadResult.errorCode = FILE_NOT_FOUND;
        loadResult.errorMessage = QString("WorldLoader File not found: ")
            .append(QFileInfo(worldFile).absoluteFilePath());
        return loadResult;
    }

    if (libraryDir.exists() == false) {
        loadResult.errorCode = LIBRARY_NOT_FOUND;
        loadResult.errorMessage = QString("WorldLoader Library dir not found: ")
            .append(libraryDir.absolutePath());
        return loadResult;
    }

    //
    // Unload libraries if this is a full reload
    //
    if (fullReload)
        sdlThread->unloadAllLibraries();

    //
    // Read world file and parse JSON
    //
    QJson::Parser parser;
    if (worldFile.open(QIODevice::ReadOnly | QIODevice::Text) == false) {
        loadResult.errorCode = IO_ERROR;
        loadResult.errorMessage = QString("Failed to open: ")
            .append(QFileInfo(worldFile).absoluteFilePath());
        return loadResult;
    }

    QByteArray jsonData = worldFile.readAll();
    worldData = parser.parse(jsonData, &ok).toList();

    worldFile.close();

    if (!ok) {
        loadResult.errorCode = JSON_PARSE_FAILED;
        loadResult.errorMessage = QString("WorldLoader failed to parse JSON in file: ")
            .append(QFileInfo(worldFile).absoluteFilePath());
        return loadResult;
    }

    // Initialize new world
    HWorld &hworld = sdlThread->getHWorld(true);
    hworld.initialize();

    foreach (QVariant rawData, worldData) {

        // TODO - Line numbers or other positional data would be nice to put in error messages

        if (rawData.type() != QVariant::Map) {
            loadResult.errorCode = BAD_META_DATA;
            loadResult.errorMessage = "WorldLoader encountered data that was not an object in world data list";
            return loadResult;
        }

        QVariantMap data = rawData.toMap();
        if (data.contains("type") == false) {
            loadResult.errorCode = BAD_META_DATA;
            loadResult.errorMessage = "WorldLoader encountered object with no type";
            return loadResult;
        }

        QString type = data["type"].toString();
        QString resource = data["resource"].toString();

        if (data.contains("attributes") == false) {
            loadResult.errorCode = BAD_META_DATA;
            loadResult.errorMessage = "WorldLoader encountered object missing attributes";
            break;
        }

        QVariantMap attributes = data["attributes"].toMap();

        if (type.compare("world", Qt::CaseInsensitive) == 0) {
            handleWorld(attributes);
        } else if (type.compare("background", Qt::CaseInsensitive) == 0) {
            handleBackground(resource, attributes);
        } else if (type.compare("background-repeat", Qt::CaseInsensitive) == 0) {
            handleBackgroundRepeat(resource, attributes);
        } else if (type.compare("actor", Qt::CaseInsensitive) == 0) {
            handleActor(resource, attributes);
        } else if (type.compare("chain", Qt::CaseInsensitive) == 0) {
            handleChain(attributes);
        }

        if (loadResult.errorCode != OK)
            break;
    }

    //
    // Initialize scripting
    //
    sdlThread->createScriptContext();
    asIScriptEngine *scr = sdlThread->scriptEngine();
    asIScriptContext *scrCtx = sdlThread->scriptContext();

    //
    // Initialize scripts that may have been loaded with world
    //

    // Offer all the actor instances to the camera script, it should hold onto
    // references to any of the instances it needs
    asIScriptModule *cameraLogicModule = sdlThread->getCameraLogicModule();
    if (cameraLogicModule != NULL) {
        qDebug() << "WorldLoader initializing camera logic script";
        asIScriptFunction *offerFunc = cameraLogicModule->GetFunctionByDecl("void onOffer(ActorInstance@)");
        if (offerFunc == NULL) {
            qDebug() << "WorldLoader WARN: found camera logic script that does not contain onOffer(ActorInstance@) function";
        } else {
            foreach (ActorInstance *actInst, hworld.actorInstances) {
                scrCtx->Prepare(offerFunc);
                scrCtx->SetArgAddress(0, actInst);
                r = scrCtx->Execute();

                if (r != asEXECUTION_FINISHED) {
                    qDebug() << "WorldLoader execute camera logic script function onOffer(ActorInstance@) failed";
                }
            }
        }
    }

    // Restart the world time clock
    sdlThread->resetWorldStartTime();

    return loadResult;
}

void WorldLoader::handleWorld(QVariantMap &data) {
    // Setup Camera
    GLdouble camX = data["camX"].toDouble();
    GLdouble camY = data["camY"].toDouble();

    sdlThread->getCamera()->x(camX);
    sdlThread->getCamera()->y(camY);
    qDebug() << "WorldLoader set camera position X:" << camX << " Y:" << camY;

    bool camDimSet = false;
    GLdouble camDim;
    if (data.contains("camWidth")) {
        camDim = data["camWidth"].toDouble();
        sdlThread->getCamera()->width(camDim);
        qDebug() << "WorldLoader set camera width in meters to " << camDim;
        camDimSet = true;
    } else if (data.contains("camHeight")) {
        camDim = data["camHeight"].toDouble();
        sdlThread->getCamera()->height(camDim);
        qDebug() << "WorldLoader set camera height in meters to " << camDim;
        camDimSet = true;
    }

    if (!camDimSet) {
        loadResult.errorCode = BAD_META_DATA;
        loadResult.errorMessage = "WorldLoader world object must specify one of camWidth or camHeight";
        return;
    }

    // Check for camera script
    if (data.contains("camLogic")) {
        QFileInfo camScriptInfo(libraryDir, data["camLogic"].toString());
        qDebug() << "WorldLoader loading camera script: " << camScriptInfo.absoluteFilePath();
        QStringList srcFiles;
        srcFiles.append(camScriptInfo.absoluteFilePath());
        ScriptUtil::instance().buildScriptModule(ASMN_CAMERA_LOGIC, srcFiles);

        asIScriptModule *cameraLogicModule = ScriptUtil::instance().getModule(ASMN_CAMERA_LOGIC);
        if (cameraLogicModule == NULL) {
            qDebug() << "WorldLoader module not found after script compile: " << ASMN_CAMERA_LOGIC;
        } else {
            sdlThread->setCameraLogicModule(cameraLogicModule);
        }
    }

    // Setup physics
    float32 gravX = data["gravX"].toFloat();
    float32 gravY = data["gravY"].toFloat();
    sdlThread->getHWorld().world()->SetGravity(b2Vec2(gravX, gravY));
    qDebug() << "WorldLoader set gravity to X:" << gravX << " Y:" << gravY;
}

void WorldLoader::handleBackground(QString resource, QVariantMap &data) {
    Background *background = backgroundAvailable(resource);
    if (background == NULL)
        return;

    GLdouble x = data["x"].toDouble();
    GLdouble y = data["y"].toDouble();
    BackgroundInstance bgInst(background, x, y);

    sdlThread->getHWorld().backgroundInstances.append(bgInst);
    qDebug() << "WorldLoader Added background " << resource << " at X:" << x << " Y:" << y;
}

void WorldLoader::handleBackgroundRepeat(QString resource, QVariantMap &data) {
    Background *background = backgroundAvailable(resource);
    if (background == NULL)
        return;

    if (background->getWidth() == 0) {
        loadResult.errorCode = BAD_META_DATA;
        loadResult.errorMessage = QString("WorldLoader asked to repeat texture with 0 width: ")
            .append(resource);
        return;
    }

    // TODO - Add support for y repeat, only supports x repeat now
    GLdouble y = data["y"].toDouble();
    GLdouble xMin = data["xMin"].toDouble();
    GLdouble xMax = data["xMax"].toDouble();

    GLdouble x = xMin;
    while (x < xMax) {
        BackgroundInstance bgInst(background, x, y);
        sdlThread->getHWorld().backgroundInstances.append(bgInst);
        x += background->getWidth();
        qDebug() << "WorldLoader Added background " << resource << " at X:" << x << " Y:" << y;
    }
}

void WorldLoader::handleActor(QString resource, QVariantMap &data) {
    Actor* actor = actorAvailable(resource);
    if (actor == NULL)
        return;

    float32 x = data["x"].toFloat();
    float32 y = data["y"].toFloat();
    //GLdouble x = data["z"].toDouble();
    QString animationState = data["animationState"].toString();

    instID += 1;
    ActorInstance *actInst = new ActorInstance(actor, instID);
    actInst->bodyDefPtr()->position.Set(x, y);

    if (data.contains("controlSet"))
        actInst->controlSet = data["controlSet"].toUInt();

    if (actInst->createBody(sdlThread->getHWorld().world()) == false) {
        loadResult.errorCode = UNEXPECTED_ERROR;
        loadResult.errorMessage = "WorldLoader could not create b2Body for brand new ActorInstance";
        return;
    }

    actInst->setState(animationState);
    sdlThread->getHWorld().actorInstances.append(actInst);
    qDebug() << "WorldLoader Added actor " << resource << "ID:" << instID << " at X:" << x << " Y:" << y;

//    foreach (ActorInstance *ai, sdlThread->getHWorld().actorInstances) {
//        QString name(ai->name());
//        name.append(QString().setNum(ai->instID()));
//        b2Vec2 pos = ai->bodyPtr()->GetPosition();
//        qDebug() << "ActorInstance " << name << " at X:"  << pos.x << " Y:" << pos.y;
//    }
}

void WorldLoader::handleChain(QVariantMap &data) {
    if (data.contains("vertices") == false) {
        loadResult.errorCode = BAD_META_DATA;
        loadResult.errorMessage = "WolrdLoader encountered chain object missing verticies list";
        return;
    }

    QVariantList vertices = data["vertices"].toList();

    int vcount = vertices.size();
    b2Vec2 *vlist = new b2Vec2[vcount];

    for (int vidx = 0; vidx < vcount; vidx++) {
        QVariantList vpair = vertices.at(vidx).toList();
        vlist[vidx].x = vpair.at(0).toFloat();
        vlist[vidx].y = vpair.at(1).toFloat();
        qDebug() << "WorldLoader added chain vertex X:" << vlist[vidx].x << " Y:" << vlist[vidx].y;
    }

    b2ChainShape chain;
    chain.CreateChain(vlist, vcount);

    b2BodyDef chainBodyDef;
    chainBodyDef.position.Set(0, 0);
    chainBodyDef.type = b2_staticBody;
    b2Body *chainBody = sdlThread->getHWorld().world()->CreateBody(&chainBodyDef);
    chainBody->CreateFixture(&chain, 0);

    delete vlist;
}

Background* WorldLoader::backgroundAvailable(QString name) {
    if (sdlThread->getBackgrounds().contains(name))
        return sdlThread->getBackgrounds().value(name);

    QString fileName(name);
    QString filePath = libraryDir.filePath(fileName);

    Background *background = loadBackgroundResource(filePath);
    if (background == NULL) {
        loadResult.errorCode = LIBRARY_RESOURCE_UNAVAILABLE;
        loadResult.errorMessage = QString("WorldLoader could not load background from ")
            .append(filePath);
        return NULL;
    }

    sdlThread->getBackgrounds().insert(background->getName(), background);
    return background;
}

Actor* WorldLoader::actorAvailable(QString name) {
    if (sdlThread->getActors().contains(name))
        return sdlThread->getActors().value(name);

    QString fileName(name);
    QString filePath = libraryDir.filePath(fileName);

    Actor *actor = loadActorResource(filePath);
    if (actor == NULL) {
        loadResult.errorCode = LIBRARY_RESOURCE_UNAVAILABLE;
        loadResult.errorMessage = QString("WorldLoader could not load actor from ")
            .append(filePath);
        return NULL;
    }

    sdlThread->getActors().insert(actor->getName(), actor);
    return actor;
}

LoadWorldTask::LoadWorldTask(SDLThread *sdlThread, const QString& filePath, const QString &libraryPath) : ExtTask("LoadWorldTask") {
    this->sdlThread = sdlThread;
    this->filePath = QString(filePath);
    this->libraryPath = QString(libraryPath);
}

void LoadWorldTask::run() {
    WorldLoader loader(sdlThread, filePath, libraryPath);
    LoadResult loadResult = loader.loadWorld(true);
    if (loadResult.errorCode != OK) {
        qDebug() << "LoadWorldTask failed with error: " << loadResult.errorMessage;
        return;
    }

    sdlThread->setPauseLoop(false);
    sdlThread->setPauseRender(false);
}
