#include <string>
#include <iostream>
#include <archive.h>
#include <archive_entry.h>

int main() {
    std::string arPath = "C:/dev/gamews/animation-scratch/testdog.zip";

    archive *ar;
    archive_entry *entry;
    int openResult;

    ar = archive_read_new();
    archive_read_support_filter_all(ar);
    archive_read_support_format_all(ar);
    openResult = archive_read_open_filename(ar, arPath.data(), 32768);

    if (openResult != ARCHIVE_OK) {
        std::cout << "Archive is invalid!" << std::endl;
        exit(1);
    }

    while (archive_read_next_header(ar, &entry) == ARCHIVE_OK) {
        std::cout << "Archive File: " << archive_entry_pathname(entry) << std::endl;
    }

    archive_read_free(ar);

    return 0;
}

