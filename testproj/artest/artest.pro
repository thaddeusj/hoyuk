TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp
LIBS += -L../../libs/debug -larchive
INCLUDEPATH += ../../libs/include
